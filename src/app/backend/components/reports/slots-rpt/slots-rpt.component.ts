import { Component } from '@angular/core';
import { LayoutService } from '../../../../layout/service/app.layout.service';
import { SlotsRptService } from './slots-rpt.service';
import { Message, MessageService } from 'primeng/api';
import { LookupService} from '../../../services/lookup.service';
import * as moment from 'moment-timezone';
import { formatDate } from '@angular/common';
import { Table } from 'primeng/table';
import { LocaleSettings } from 'primeng/calendar';
import * as xlsx from 'xlsx';

@Component({
  selector: 'app-slots-rpt',
  templateUrl: './slots-rpt.component.html',
  styleUrls: ['./slots-rpt.component.scss'],
  providers: [MessageService]
})
export class SlotsRptComponent {

  locale: LocaleSettings = {
    firstDayOfWeek: 0,
    dayNames: ["อาทิตย์", "จันทร์", "อังคาร", "พุธ", "พฤหัส", "ศุกร์", "เสาร์"],
    dayNamesShort: ["อา", "จ", "อ", "พ", "พฤ", "ศ", "ส"],
    dayNamesMin: ["อา", "จ", "อ", "พ", "พฤ", "ศ", "ส"],
    monthNames: [
        "มกราคม",
        "กุมภาพันธ์",
        "มีนาคม",
        "เมษายน",
        "พฤษภาคม",
        "มิถุนายน",
        "กรกฎาคม",
        "สิงหาคม",
        "กันยายน",
        "ตุลาคม",
        "พฤศจิกายน",
        "ธันวาคม"
    ],
    monthNamesShort: [
        "ม.ค.",
        "ก.พ.",
        "มี.ค.",
        "เม.ย.",
        "พ.ค.",
        "มิ.ย.",
        "ก.ค.",
        "ส.ค.",
        "ก.ย.",
        "ต.ค.",
        "พ.ย.",
        "ธ.ค."
    ],
    today: 'Today',
    clear: 'Clear',
    dateFormat: 'dd/mm/yy',
    weekHeader: 'Wk'
};

  dateFrom: Date = new Date();
  dateTo: Date = new Date();

  blockedPanel: boolean = false;
  msgs: Message[] = [];
  messages: any | undefined;
  hospitalId = sessionStorage.getItem('hospitalId');
  serviceId = sessionStorage.getItem('serviceId');
  roleId = sessionStorage.getItem('userRole');

  slots: any;

  constructor(
    private layoutService: LayoutService,
    private slotsrptService:SlotsRptService,
    private messageService: MessageService,
    private lookupService:LookupService,
    ) {

    }

    async ngOnInit() {
      // await this.getService();
      // await this.getServiceType();
      // await this.getPeriod();        
      await this.getData();
  }

  onGlobalFilter(table: Table, event: Event) {
    table.filterGlobal((event.target as HTMLInputElement).value, 'contains')
}

thaiDateFormat(date:any){
  const thaiMonth = [
      'ม.ค.','ก.พ.','มี.ค.','เม.ย.','พ.ค.','มิ.ย.','ก.ค.','ส.ค.','ก.ย.','ต.ค.','พ.ย.','ธ.ค.'
  ]
  const newDate = new Date(date);
  let day = newDate.getDate();
  let monthName = thaiMonth[newDate.getMonth()];
  let thaiYear = newDate.getFullYear() + 543;

  let thaidate = day + ' ' + monthName + ' ' + thaiYear;
  return thaidate;
  
}

async getData() {
  this.blockedPanel = true;
// console.log("serviceID",this.serviceId);
// console.log("hospitalId",this.hospitalId);
// console.log("roleId",this.roleId)
  //set date to Y-m-d
  let dateFrom = this.dateFrom.getTime() + 25200000;
  let IsoDateFrom = new Date(dateFrom).toISOString().split('T')[0];
  let dateTo = this.dateTo.getTime() + 25200000;
  let IsoDateTo = new Date(dateTo).toISOString().split('T')[0];

  // load data
  try{
      const res:any  = await this.slotsrptService.getReportSlots(IsoDateFrom,IsoDateTo);
      let slotss = [];
      slotss = res.data;
      // console.log(slotss);
      
      if(slotss.results.rows.length > 0){
          this.slots = slotss.results.rows;  
          // console.log(this.slots);
             
          if(this.roleId == '2' || this.roleId == '3'){
            /* fiter for userlogin hospital_id */
            let _slots = this.slots.filter(
                (s: any) =>
                    s.hospital_id == this.hospitalId
            );
            this.slots = _slots;
            // console.log("fon1");
            
          }

          if(this.roleId == '3'){
            /* fiter for userlogin service_id */
            let _slots = this.slots.filter(
                (s: any) =>
                    s.service_id == this.serviceId
            );
            this.slots = _slots;
            // console.log("fon2");
            
          }

      
      } else {
          // console.log("data is null");
      }
      this.blockedPanel = false;
  }catch(error) {
      this.blockedPanel = false;
      console.log(error);
  }
}

searchData(){
  this.getData();

}

exportExcel()
{
  let excel:any =[];
  let i:any = 0;
  for(let s of this.slots){
    i++
    let data = {
      'ลำดับ' : i,
      'หน่วยบริการ' : s.hospital_name,
      'คลินิก' : s.service_name,
      'บริการ' : s.service_type_name,
      'ชื่อสล็อต' : s.slot_name,
      'วันที่' : this.thaiDateFormat(s.slot_date),
      'ช่วงเวลา' : s.period_name,
      'จำนวน' : s.total_slot_per_period,

    }
    excel.push(data);
  }
  const ws: xlsx.WorkSheet = xlsx.utils.json_to_sheet(excel);
  const wb: xlsx.WorkBook = xlsx.utils.book_new();
  xlsx.utils.book_append_sheet(wb, ws, 'Sheet1');
  xlsx.writeFile(wb, 'slots_rpt.xlsx');
}

}

