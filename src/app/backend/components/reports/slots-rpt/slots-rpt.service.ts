import { Injectable } from '@angular/core';
import axios from 'axios';
import { environment } from '../../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SlotsRptService {

  private axiosInstance = axios.create({
    baseURL: `${environment.apiUrl}`,
});

  constructor() { 
    this.axiosInstance.interceptors.request.use((config) => {
      const token = sessionStorage.getItem('token');

      if (token) {
          config.headers['Authorization'] = `Bearer ${token}`;
      }
      return config;
  });
  }

  async getReportSlots(startDate:any,endDate:any) {
    // const url = `/report/slot/`+ id;
    // const url = `/report/slot/12/12/2024-01-01/2024-01-01`;
    const url: any = `/report/slot/` + startDate + `/` + endDate;
    return this.axiosInstance.get(url);
}

}
