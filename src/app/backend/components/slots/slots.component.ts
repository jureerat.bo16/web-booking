import { Component,OnInit } from '@angular/core';
import { LayoutService } from '../../../layout/service/app.layout.service';
import { SlotsService } from './slots.service';
import { LookupService} from '../../services/lookup.service';
import { Table } from 'primeng/table';
import { Message, MessageService } from 'primeng/api';
import * as moment from 'moment-timezone';
import { formatDate } from '@angular/common';

interface AutoCompleteCompleteEvent {
    originalEvent: Event;
    query: string;
}
@Component({
    selector: 'app-backend-slots',
    templateUrl: './slots.component.html',
    styleUrls: ['./slots.component.scss'],
    providers: [MessageService]
})
export class SlotsComponent {
    //ประกาศตัวแปร
    blockedPanel: boolean = false;
    slotidIdEdit : number = 0;   
    hospitalId = sessionStorage.getItem('hospitalId');
    selectedService: any | undefined;
    selectedServicetype: any | undefined;
    selectedPeriod: any | undefined;
    isAdd :boolean = false;
    isEdit :boolean = false;
    displayForm: boolean = false;
    clinicNameEdit :string = '';
    slots: any;
    listPeriods = [];
    listService:any = [];
    listServicetype:any = [];
    listServicetypetemp:any = [];
    periodNames :string = '';
    status_is_active :boolean = true;
    slot_avialable :boolean = true;
    total_slot_per_period:number = 0;
    slot_date1 : Date =  new Date();
    slot_date : any;
    slot_name : any;
    date: Date | undefined;
    messages: any | undefined;
    isSpiner: boolean = false;
    isValidations: boolean = true;

    constructor(
            private layoutService: LayoutService,
            private slotsService:SlotsService,
            private lookupService:LookupService,
            private messageService: MessageService
            ) 
        {
        }

    scrollTo(viewChild: HTMLElement) {
        viewChild.scrollIntoView({behavior: 'smooth'});
    }

    async ngOnInit() {
        await this.getService();
        await this.getServiceType();
        await this.getPeriod();        
        await this.getData();       
    }
    // alert message
    showMessages(severity: any, summary: any, detail: any) {
        this.messages = [
            { severity: severity, summary: summary, detail: detail },
        ];
    }
  // alert showToast
    showToast(severity: any, summary: any, detail: any) {
        this.messageService.add({ severity: severity, summary:summary, detail: detail });
    }
    clearMessages() {
        this.messages = [];
    }

    onGlobalFilter(table: Table, event: Event) {
        table.filterGlobal((event.target as HTMLInputElement).value, 'contains')
    }

    selectClinic() :void {
        let dataselect = this.selectedService;
        this.listServicetypetemp = this.listServicetype.filter((s:any) => s.service_id === dataselect);
    }

    async getPeriod() {
        const res:any = await this.lookupService.listPeriodid(this.hospitalId);
        let period = [];
        period = res.data;
        if(period.results.length > 0){
            this.listPeriods = period.results;
        } else {
            // console.log("error load period");
        }
    }
    async getService() {
        const res:any = await this.lookupService.listService();
        let service = [];
        service = res.data;
        if(service.results.length > 0){
            this.listService = service.results;
        } else {
            // console.log("error load service");
        }
    }
    async getServiceType() {
        const res:any = await this.lookupService.listServiceType();
        let servicetype = [];
        servicetype = res.data;
        if(servicetype.results.length > 0){
            this.listServicetype = servicetype.results;
            this.listServicetype = this.listServicetype.filter((s:any) => s.is_active);
            this.listServicetypetemp = this.listServicetype;     
        } else {
            // console.log("error load servicetype");
        }        
    }
        //วันที่
        thaiDateFormat(date:any){
            const thaiMonth = [
                'ม.ค.','ก.พ.','มี.ค.','เม.ย.','พ.ค.','มิ.ย.','ก.ค.','ส.ค.','ก.ย.','ต.ค.','พ.ย.','ธ.ค.'
            ]
            const newDate = new Date(date);
            let day = newDate.getDate();
            let monthName = thaiMonth[newDate.getMonth()];
            let thaiYear = newDate.getFullYear() + 543;

            let thaidate = day + ' ' + monthName + ' ' + thaiYear;
            return thaidate;
            
        }
    async getData() {
        this.blockedPanel = true;
        try{
            const res:any  = await this.slotsService.listdataByid(this.hospitalId);
            let slotss = [];
            slotss = res.data;
            
            if(slotss.results.length > 0){
                this.slots = slotss.results;         
                for(let s of this.slots){
                    let p:any = this.listPeriods.find((p:any) => p.period_id === s.period_id);
                    s.period_name = p.period_name;
                }
                for(let a of this.slots){             
                    let c:any = this.listService.find((c:any) => c.service_id === a.service_id);
                    a.service_name = c.service_name;
                }
                for(let x of this.slots){             
                    let c:any = this.listServicetypetemp.find((c:any) => c.service_type_id === x.service_type_id);
                    x.service_type_name = c.service_type_name;
                }
               
            } else {
                // console.log("data is null");
            }
            this.blockedPanel = false;
        }catch(error) {
            this.blockedPanel = false;
            console.log(error);
        }
    }

    // เพิ่มรายการ
    displayFormAdd() {
        this.isAdd = true;
        this.isEdit = false;
        this.displayForm = true;
        this.slot_name = "",
        this.selectedService = "",
        this.selectedServicetype = "",
        this.selectedPeriod = "",
        this.slot_date = new Date(),
        this.total_slot_per_period = 0,
        this.slot_avialable = true,
        this.status_is_active = true
        
    }      
      // function save data
    async save() {
        this.messages = "";
        if(this.selectedService == '' || this.selectedService == undefined){
            this.isValidations = false;
            this.messages += " *ไม่บันทึกคลินิก";
        }else if(this.selectedServicetype == '' || this.selectedServicetype == undefined){
            this.isValidations = false;
            this.messages += " *ไม่บันทึกการให้บริการ";
        }
        else if(this.slot_name == '' || this.slot_name == undefined){
            this.isValidations = false;
            this.messages += " *ไม่บันทึกชื่อสลอต";
        }

        else if(this.selectedPeriod == '' || this.selectedPeriod == undefined){
            this.isValidations = false;
            this.messages += " *ไม่บันทึกช่วงเวลา";
        }
        else if(this.slot_date == undefined){
            this.isValidations = false;
            this.messages += " *ไม่บันทึกวันที่เปิดสลอต";
        }
        else if(this.total_slot_per_period == undefined){
            this.isValidations = false;
            this.messages += " *ไม่บันทึกจำนวนสลอต";
        }else{
            this.isValidations = true;
            this.messages = "";
        }

        try{
            if(this.isValidations) {
                this.isSpiner = true;
                let data = {            
                    slot_name: this.slot_name,
                    hospital_id: this.hospitalId,
                    service_id: this.selectedService,
                    service_type_id: this.selectedServicetype,
                    period_id: this.selectedPeriod,
                    slot_date : moment(this.slot_date).tz('Asia/Bangkok').format('YYYY-MM-DD'),
                    total_slot_per_period: this.total_slot_per_period,
                    slot_avialable: this.slot_avialable,
                    is_active: this.status_is_active
                };
                
                 //save data
                    const res:any = await this.slotsService.save(data);
                    this.showToast('success','Success','บันทึกสำเร็จ' )
                    // close form
                    this.displayForm = false;
                    
            
            //refresh data
            await this.getData();
            this.isSpiner = false;
            }else{
                
                this.showMessages('error','Error',this.messages );
            }


        }catch(error) {
            console.log(error);
        }       
        
    }   
    // display form edit data
    displayFormEdit(data: any) {
        this.isAdd = false;
        this.isEdit = true;
        this.slotidIdEdit = data.slot_id;
        this.slot_name = data.slot_name,
        this.selectedService = data.service_id,
        this.selectedServicetype = data.service_type_id,        
        this.selectedPeriod = data.period_id,
        this.slot_date = new Date(data.slot_date),
        this.total_slot_per_period = data.total_slot_per_period,
        this.slot_avialable = data.slot_avialable,
        this.status_is_active = data.is_active
        this.displayForm = true;
    }
    async update() {
        try{
            this.isSpiner = true;
            let id = this.slotidIdEdit;
            let body = {
                slot_name: this.slot_name,
                hospital_id: this.hospitalId,
                service_id: this.selectedService,
                service_type_id: this.selectedServicetype,
                period_id: this.selectedPeriod,
                slot_date : moment(this.slot_date).tz('Asia/Bangkok').format('YYYY-MM-DD'),
                total_slot_per_period: this.total_slot_per_period,
                slot_avialable: this.slot_avialable,
                is_active: this.status_is_active
            };

            // update data
            const res:any = await this.slotsService.update(id, body);
            let message_upd = [];
                message_upd = res.data;
                this.showToast('success','Success','แก้ไขสำเร็จ' )
            this.displayForm = false;
            await this.getData();
            this.isSpiner = false;
        }catch(err){

        }
        
    }

    get backgroundStyle(): object {
        let path = 'assets/demo/images/landing/';
        let image = this.layoutService.config.colorScheme === 'dark' ? 'line-effect-dark.svg' : 'line-effect.svg'; 

        return {'background-image': 'url(' + path + image + ')'};
    }

    get colorScheme(): string {
        return this.layoutService.config.colorScheme;
    }

}