import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppointCardComponent } from './appoint-card.component';

const routes: Routes = [];

@NgModule({
  imports: [RouterModule.forChild([
    { path: '', component: AppointCardComponent }
])],
  exports: [RouterModule]
})
export class AppointCardRoutingModule { }
