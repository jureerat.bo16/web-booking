import { Injectable } from '@angular/core';
import axios from 'axios';
import { environment } from '../../../../../environments/environment';

@Injectable({
    providedIn: 'root',
})
export class AppointCardService {
    // pathPrefix: any = `:40014`
    // pathPrefixAuth: any = `:40001`;

    private axiosInstance = axios.create({
        baseURL: `${environment.apiUrl}`,
    });

    constructor() {
        this.axiosInstance.interceptors.request.use((config) => {
            const token = sessionStorage.getItem('token');
            if (token) {
                config.headers['Authorization'] = `Bearer ${token}`;
            }
            return config;
        });
    }

  
    async getById(id: number) {
        const url = `/reserves/getByID/` + id;
        return this.axiosInstance.get(url);
    }

    async getHospitalById(id:number) {
        const url = `/hospitals/getByID/`+ id;
        return this.axiosInstance.get(url);
    }

    async getSlotById(id:number) {
        const url = `/slots/getByID/`+ id;
        return this.axiosInstance.get(url);
    }

    async getCustomerById(id:number) {
        const url = `/customers/getByID/`+ id;
        return this.axiosInstance.get(url);
    }

    async getPeriodById(id:number) {
        const url = `/periods/getByID/`+ id;
        return this.axiosInstance.get(url);
    }

    async getServiceTypeById(id:number) {
        const url = `/service_types/getByID/`+ id;
        return this.axiosInstance.get(url);
    }

    async getProfilesById(id: any) {
        const url = `/profiles/getByID/` + id;
        return this.axiosInstance.get(url);
    }


}